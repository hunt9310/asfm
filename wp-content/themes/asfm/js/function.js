jQuery(document).ready( function() {
  (function ($) {

		$(function() {
		  var top_header = '';
		  $(document).ready(function(){
		    top_header = $('.ms-slide-bgcont img');	 
		    second_header = $('.background_image img');
		  });
		  $(window).scroll(function () {
		    var st = $(window).scrollTop();
		    top_header.css({'top':(0+st/2)+"px"});
		    second_header.css({'top':(-100+st*.5)+"px"});
		  })
		});

		var toggle = $('.toggle');
		var nav = $('.main-navigation');
		var brand = $('.site-branding');

		$(toggle).click(function(){
		    $(this).toggleClass('active');
		    $(nav).toggleClass('active');
		    $(brand).toggleClass('fixed');
		});

	})(jQuery);
});