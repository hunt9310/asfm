<?php
/**
 * Template Name: Second Page
 */

get_header(); ?>

	<div class="row" id="second">
		<div class="col-sm-12">
		<?php if ( has_post_thumbnail()) : ?>
		   <div class="top_area">
			    <div class="background_image"><?php the_post_thumbnail(); ?></div>
		   </div>
		 <?php endif; ?>

		  <div class="content_section">
				<div class="text_section">
					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'second' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>
				</div>					
			</div>
		</div> <!-- /.col -->
	</div> <!-- /.row -->

<?php get_footer(); ?>