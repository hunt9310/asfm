<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package asfm
 */

get_header(); ?>

	<div class="row" id="second">
		<div class="col-sm-12">
		   <div class="top_area">
			    <div class="background_image"><img src="http://localhost:8888/asfm/wp-content/uploads/2017/02/middle_bg.png" /></div>
		   </div>

		  <div class="content_section">
				<div class="text_section">
					<h2 class="entry-title">Search</h2>
					<div class="entry-content">
						<?php
						if ( have_posts() ) : ?>

							<header class="page-header">
								<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'asfm' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
							</header><!-- .page-header -->

							<?php
							/* Start the Loop */
							while ( have_posts() ) : the_post();

								/**
								 * Run the loop for the search to output the results.
								 * If you want to overload this in a child theme then include a file
								 * called content-search.php and that will be used instead.
								 */
								get_template_part( 'template-parts/content', 'search' );

							endwhile;

							the_posts_navigation();

						else :

							get_template_part( 'template-parts/content', 'none' );

						endif; ?>
					</div>
					<?php get_sidebar(); ?>

				</div>
			</div>
		</div>
	</div>

<?php
get_footer();
