<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package asfm
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="logo">
				<img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/uploads/2017/02/logo_white.png" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">			
			</a>
			<div class="contact">
				<div class="top">200 W. Ridge Road Suite 450<br />
				Rochester, NY 14615</div>
				<div class="bottom">585.254.3333<br />
				<a href="mailto:info@armorsecurity.org">info@armorsecurity.org</a></div>
			</div>
			<div class="footer-nav">
				<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
			</div>
			<div class="quote">
				<h1>Request a Quote</h1>
				<p>Want to learn how ASFM can help you Cannabis Company? Contact us today for a Quote!</p>
				<a href="#" class="button">Get a Quote</a>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
