<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'asfm');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1(_x>:l=.t2NiG-rW_e-sHJjD8HnC} 6v@1miS3p,ccsO?v[7o4{oG(t[dJS.8+P');
define('SECURE_AUTH_KEY',  'fj1dR^pjW`;/+J]X)L^R]1bTsKl>Qb.4AW Jl/|qH+E?,YvN-KyQtatb,N).f|o#');
define('LOGGED_IN_KEY',    'ySU@FAg[WPX[Co*]OpS8n.}xGTxQ+-Ua|U}lk|u_vn;@EH-Uy,.2| $Kfp=X#q(P');
define('NONCE_KEY',        '{x<HOQ)TQ:j9k;R=Q+-O[5J-d&Q0N2P&+dO>,T!IUj`vD1&h;,dgW~PDUZ~|N&-r');
define('AUTH_SALT',        'WTT*2oBtD[y@imZ>l6Ut;G~=]RR=Gbysy]:C6a?}gA9jRTlE.L%SD]}<FM<Fid#E');
define('SECURE_AUTH_SALT', 'K{2dCG+uC-}4_/aN-I!Q+uI_vSk+SaF`-m}az.gs}Dh4+%6yoj/ZMn-n(]9_EiR.');
define('LOGGED_IN_SALT',   '}.0kex]Swq$u|v[7D,0]Cx.NB2{Aa<]+|5DTM*BZ2+|9M-X9f^!Um:w?sma`iCQn');
define('NONCE_SALT',       'g|zK{OC|;-c<h>BoFy>z9*+2n+ode*StCoVynv% sCK-x6Z_<Y.*bEVt[-6</`[,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'asfm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
